 # -*- coding: utf-8 -*-
#!/usr/bin/env python
#!/usr/bin/python -u

#Voltage controlled astable tuned oscillator

import sys, getopt

import control
import matplotlib.pyplot as plt

import sympy
import cmath
import math


sys.path.append('../SUBM/ICsE/SOFT/')
import ICsE


import TFAN

import IAMP

#Funcion definition

def VCATO_tFaC():#Voltage controled astable tunned oscillator transfer function analytic calculation
	#IAwD_CC=sympy.factor(IAMP.IAwD_tFaC()*IAMP.CC_tFaC())
	IAwD_CC=IAMP.IAwD_CC_tFaC()
	IAwD_CC_p_EIC=sympy.factor(IAwD_CC+ICsE.ICsE_tFaC())
	# print("IAwD_CC_p_EIC=",IAwD_CC_p_EIC)
	#IAwD_CC_p_EIC_COEF=OSCGF.all_coeffs()

	CSf=sympy.factor(IAwD_CC/(1-(IAwD_CC*IAwD_CC_p_EIC)))
	# print("CSf=",CSf)

	return CSf


def VCATO_tfC(g_m,r_d,c_d,c_cc,r_g,c_s,l_s,r_s,r_o):#Voltage controled astable tunned oscillator transfer function calculation

	IAwD_CC=IAMP.IAwD_CC_tfC(g_m,r_d,c_d,c_cc,r_g)

	IAwD_CC_p_ICTf=control.parallel(IAwD_CC,ICsE.ICsE_tFC(c_s,l_s,r_s,r_o))

	CS=IAwD_CC/(1-IAwD_CC*IAwD_CC_p_ICTf)

	return(CS)

def VCATO_gC(g_m,r_d,c_d,c_cc,r_g,c_s,l_s,r_s,r_o):#VCATO gain calcultaion to have stable oscillation

	def rcp(G_m):
		VCATOTF=VCATO_tfC(float(G_m),r_d,c_d,c_cc,r_g,c_s,l_s,r_s,r_o)
		p = control.pole(VCATOTF)
		for i in range(len(p)):
			if p[i].imag!=0:
				if(p[i].real!=0):
					return(abs(p[i].real))
				else:
					return 0

		#return(abs(G_m+0.1))


	print("rcp=",rcp(1.00001))

	from scipy import optimize

	minres=optimize.minimize(rcp,x0=1,bounds=optimize.Bounds(lb=0.999, ub=1.001))
	print("minres=",minres)

	print("rcp=",rcp(minres.x))

	G_M=minres.x

	return G_M





#System variables
g_m=1.0000436536573636
R_d=1
C_d=1*10**-14
R_g=10000
C_cc=1*10**-8
R_S=1
L_S0=46*10**-9
mur=1
L_S=L_S0*mur
epr=1
C_S0=2.5*10**-12
C_SV=1.5*10**-12
C_S=C_S0*epr+C_SV
R_o=1

TSTP=1*10**(-7)

#
# g_m=1.1
# R_d=1
# C_d=1*10**-4
# R_g=10000
# C_cc=1*10**4
# R_S=1
# L_S0=1*10**1
# mur=1
# L_S=L_S0*mur
# epr=1
# C_S0=2.5*10**1
# C_SV=1.5*10**1
# C_S=C_S0*epr+C_SV
# R_o=1
#
# TSTP=1000


def main(argv):

	VCATO=VCATO_tFaC()
	TFAN.aTF_ANLS(VCATO)

	#Functon Calculations
	#VCATO=sympy.simplify(VCATO)

	print("\n","VCATO=",VCATO,"\n")

	#VCATO=VCATO.subs(sympy.Symbol("g_m"),1)
	VCATO=VCATO.subs(sympy.Symbol("R_S"),0)
	VCATO=sympy.factor(VCATO)
	fractd, fractn=sympy.fraction(VCATO)
	print()
	print("fractd",fractd)
	print("fractn",fractn)
	print()

	TFAN.aTF_ANLS(VCATO)


	print("\n","VCATO=",VCATO,"\n")

	#sprint("\n ILT",sympy.inverse_laplace_transform(VCATO, sympy.Symbol("s"), sympy.Symbol("t")),"\n")


	g_m=1.0000436536573636
	#g_m=1
	R_d=1
	C_d=1*10**-14
	R_g=1000
	C_cc=1*10**-8
	R_S=1
	L_S0=46*10**-9
	mur=1
	L_S=L_S0*mur
	epr=1
	C_S0=2.5*10**-12
	C_SV=1.5*10**-12
	C_S=C_S0*epr+C_SV
	print("C_S",C_S)
	R_o=1

	TSTP=1*10**(-6)

	VCATO=VCATO_tfC(g_m,R_d,C_d,C_cc,R_g,C_S,L_S,R_S,R_o)

	#VCATO_gC(g_m,R_d,C_d,C_cc,R_g,C_S,L_S,R_S,R_o)

	print("VCATO(s)=",VCATO)


	z = control.zeros(VCATO)
	p = control.poles(VCATO)
	print("Zeroes ")
	print(z)
	print("Poles ")
	print(p)

	control.bode_plot(VCATO)
	control.bode_plot(ICsE.ZICsE_tFC(C_S,L_S,R_S))
	control.bode_plot(IAMP.IAwD_CC_tfC(g_m,R_d,C_d,C_cc,R_g))

	IAwD_CC=IAMP.IAwD_CC_tfC(1,R_d,C_d,C_cc,R_g)
	ASTBOSC=(IAwD_CC/(1-(IAwD_CC*IAwD_CC)))

	control.bode_plot(ASTBOSC)

	plt.grid()
	plt.show()

	#control.nyquist_plot(CS)

	#print(control.frequency_response(CS,range(2*math.pi*1*10**8,2*math.pi*1*10**9,2*math.pi*100*10**6)))


	ta,ya=control.step_response(ASTBOSC,TSTP)
	plt.plot(ta, ya)
	plt.title("Step Response")
	plt.xlabel("t")
	plt.ylabel("y")
	plt.grid()
	plt.show()

	# t, y = control.impulse_response(CS,TSTP)
	# t,y=control.step_response(VCATO,TSTP)
	# plt.plot(t, y)
	# plt.title("Step Response")
	# plt.xlabel("t")
	# plt.ylabel("y")
	# plt.grid()
	# plt.show()

if __name__ == "__main__":
	main(sys.argv[1:])
