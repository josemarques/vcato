 # -*- coding: utf-8 -*-
#!/usr/bin/env python
#!/usr/bin/python -u

#Inverter RF amplifier

import sys, getopt

import control
import matplotlib.pyplot as plt

import sympy
import math

import TFAN

#Funcion definition
def ELEMORE_DELAY_tFaC():#Elemore delay transfer function analitic calculation
	s=sympy.Symbol("s",real=True)
	R_d=sympy.Symbol("R_d",real=True)
	C_d=sympy.Symbol("C_d",real=True)
	V_in=sympy.Symbol("V_in",real=True)

	Z_Cd=1/(s*C_d)
	Z_Rd=R_d

	I=V_in/(Z_Cd+Z_Rd)

	V_out=Z_Cd*I

	TF=V_out/V_in
	TF=sympy.simplify(TF)

	#print("ELEMORE_DELAY=",TF)

	return TF

def IAwD_tFaC():#Inverter amplyfier with delay transfer function analytic calculation

	#s=sympy.Symbol("s")
	#R_d=sympy.Symbol("R_d",real=True)
	#C_d=sympy.Symbol("C_d",real=True)
	g_m=sympy.Symbol("g_m",real=True)

	#V_IN=sympy.Symbol("V_IN")
	#Performance Analysis of Voltage Controlled Ring Oscillators (doi:10.1007/978-981-10-0755-2_4) "Errata on model"
	#AMP=-g_m
	#Z_Cd=1/(C_d*s)
	#I=V_IN/(R_d+Z_Cd)
	#V_OUT=I*Z_Cd

	#IAwD=sympy.factor(V_OUT/V_IN)

	IAwD=-g_m*ELEMORE_DELAY_tFaC()

	#IAwD=(-g_m*R_d)/(1+s*R_d*C_d)
	#print("IAwD=",IAwD)
	return IAwD

def CC_tFaC():#Capacitive coupling transfer function analytic calculation

	s=sympy.Symbol("s",real=True)
	C_cc=sympy.Symbol("C_cc",real=True)
	R_g=sympy.Symbol("R_g",real=True)

	V_IN=sympy.Symbol("V_IN")
	Z_Ccc=1/(C_cc*s)
	Z_Rg=R_g
	I=V_IN/(Z_Ccc+Z_Rg)
	V_OUT=I*Z_Rg

	CC=sympy.factor((V_OUT/V_IN))
	#print("CC=",CC)

	return CC

def IAwD_CC_tFaC():#Inverter amplyfier with delay and capacitive coupling transfer fuction calculation analitic calculation
	return(IAwD_tFaC()*CC_tFaC())

def ELEMORE_DELAY_tFC(r_d,c_d):#Elemore delay transfer function calculation
	ELEMORE_DELAYn=[1]
	ELEMORE_DELAYd=[r_d*c_d,1]
	ELEMORE_DELAY=control.tf(ELEMORE_DELAYn,ELEMORE_DELAYd)

	return ELEMORE_DELAY

def IAwD_tfC(g_m,r_d,c_d):#Inverter amplyfier with delay transfer function calculation

	IAn=[-g_m]
	IAd=[1]
	IA=control.tf(IAn,IAd)
	IAwD = control.series(IA,ELEMORE_DELAY_tFC(r_d,c_d))
	#print("IAwD(s)=",IAwD)

	return IAwD

def CC_tfC(c_cc,r_g): # Capacitive coupling transfer function calculation

	CCn=[c_cc*r_g,0]
	CCd=[c_cc*r_g,1]
	CC=control.tf(CCn,CCd)
	#print("CC(s)=",CC)

	return(CC)


def IAwD_CC_tfC(g_m,r_d,c_d,c_cc,r_g):#Inverter amplyfier with delay and capacitive coupling transfer fuction calculation
	IAwD_CC=control.series(IAwD_tfC(g_m,r_d,c_d),CC_tfC(c_cc,r_g))
	#print("IAwD_CC(s)=",IAwD_CC)
	return(IAwD_CC)

#System variables
g_m=1.1
R_d=50
C_d=3.2*10**-12
R_g=470
C_cc=6*10**-9

TSTP=1*10**(-8)

def TEST(argv):
	print("IAMP_TEST")

	print("ELEMORE_DELAY_tFaC():")
	print(ELEMORE_DELAY_tFaC())

	print("IAwD_tFaC():")
	print(IAwD_tFaC())

	print("CC_tFaC():")
	print(CC_tFaC())

	print("IAwD_CC_tFaC():")
	print(IAwD_CC_tFaC())

	print("Astable Oscillator:")
	print(IAwD_CC_tFaC()/(1-(IAwD_CC_tFaC()*IAwD_CC_tFaC())))

	########## Transfer functions control
	print("ELEMORE_DELAY_tFC(R_d,C_d)=",ELEMORE_DELAY_tFC(R_d,C_d))

	#IAwD_CC=IAwD*CC
	IAwD_CC=IAwD_CC_tfC(g_m,R_d,C_d,C_cc,R_g)
	print("IAwD_CC(s)=",IAwD_CC)

	#Analysis
	z = control.zeros(IAwD_CC)
	p = control.poles(IAwD_CC)
	print("Zeroes ")
	print(z)
	print("Poles ")
	print(p)

	ASTBOSC=(IAwD_CC/(1-(IAwD_CC*IAwD_CC)))
	print("ASTBOSC(s)=",ASTBOSC)

	z = control.zeros(ASTBOSC)
	p = control.poles(ASTBOSC)
	print("Zeroes ")
	print(z)
	print("Poles ")
	print(p)

	control.bode_plot(IAwD_CC)
	control.bode_plot(ASTBOSC)
	plt.grid()
	plt.show()



	#t, y = control.impulse_response(ASTBOSC,TSTP)
	t,y=control.step_response(ASTBOSC,TSTP)
	#print("y",y)
	#print("t",t)
	t= t[1:]#Remove initial point
	y= y[1:]#Remove initial point
	plt.plot(t, y)
	plt.title("Step Response")
	plt.xlabel("t")
	plt.ylabel("y")
	plt.grid()
	plt.show()

	#control.nyquist_plot(CS)

if __name__ == "__main__":
	TEST(sys.argv[1:])
