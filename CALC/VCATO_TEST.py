 # -*- coding: utf-8 -*-
#!/usr/bin/env python
#!/usr/bin/python -u

#Voltage controlled astable tuned oscillator

import sys, getopt

import control
import matplotlib.pyplot as plt

import sympy
import cmath
import math

import sys
sys.path.append('VCATO/SOFT/')
import PICRT

#Funcion definition
def Z_PICRT_aCI():#PICRT analitic calculation initialization
	omega=sympy.Symbol("omega",real=True)
	ir=sympy.Symbol("ir",real=True)
	lt=sympy.Symbol("lt",real=True)
	mu=sympy.Symbol("mur",real=True)-sympy.Symbol("mui",real=True)*sympy.I
	mui=sympy.Symbol("mui",real=True)
	ct=sympy.Symbol("ct",real=True)
	epsilon=sympy.Symbol("epsilonr",real=True)-sympy.Symbol("epsiloni",real=True)*sympy.I
	c0=sympy.Symbol("c0",real=True)
	c1=sympy.Symbol("c1",real=True)
	c2=sympy.Symbol("c2",real=True)
	v_vc=sympy.Symbol("v_vc",real=True)

	VCORT=PICRT.PICRT(omega, ir, lt, mu, 0, ct, epsilon, 0, [c0,c1,c2], v_vc)# VCO Resonant tank

	#Analitic basig impedances calculation
	VCORT.Z_LPT=VCORT.omega*sympy.I*VCORT.LPT*VCORT.mu#Phisical auto inductance
	VCORT.Z_IR=VCORT.IR#Phisical auto inductance
	VCORT.Z_CPT=1/(VCORT.omega*sympy.I*VCORT.CPT*VCORT.epsilon)#Phisical capacitance
	VCORT.VP.VPC()#Varactor capacitance calculation
	VCORT.Z_CVPC=1/(VCORT.omega*sympy.I*VCORT.VP.VC)#Varactor capacitance

	return(VCORT)

def Z_PICRT_aC():#PICRT impedance analitic calculation

	VCORT=Z_PICRT_aCI()

	Z_PICRT=VCORT.IMP()

	return(Z_PICRT)

def SRF_PICRT_aC():#PICRT self resonant frequency analitic calculation
	IM_Z_PICRT=sympy.factor(sympy.im(Z_PICRT_aC()))
	#print("\n","IM_Z_PICRT=",IM_Z_PICRT,"\n")

	#omega=sympy.Symbol("omega",real=True)
	#SRF=sympy.solve(IM_Z_PICRT,omega, simplify=False, manual=True,rational=False)
	#print("\n","SRF=",SRF,"\n")

	VCORT=Z_PICRT_aCI()

	VCORT.mur=sympy.Symbol("mur",real=True)
	VCORT.epsilonr=sympy.Symbol("epsilonr",real=True)

	SRF_PICRT=sympy.factor(VCORT.SRF())

	return(SRF_PICRT)

#System variables
g_m=1.0000436536573636
R_d=1
C_d=1*10**-14
R_g=10000
C_cc=1*10**-8
R_S=1
L_S0=46*10**-9
mur=1
L_S=L_S0*mur
epr=1
C_S0=2.5*10**-12
C_SV=1.5*10**-12
C_S=C_S0*epr+C_SV
R_o=1

TSTP=1*10**(-7)

#
# g_m=1.1
# R_d=1
# C_d=1*10**-4
# R_g=10000
# C_cc=1*10**4
# R_S=1
# L_S0=1*10**1
# mur=1
# L_S=L_S0*mur
# epr=1
# C_S0=2.5*10**1
# C_SV=1.5*10**1
# C_S=C_S0*epr+C_SV
# R_o=1
#
# TSTP=1000


def main(argv):

	#print("\n","Z_PICRT_aC()=",sympy.simplify(Z_PICRT_aC()),"\n")

	#print("\n","SRF_PICRT_aC()=",sympy.simplify(SRF_PICRT_aC()),"\n")



if __name__ == "__main__":
	main(sys.argv[1:])
