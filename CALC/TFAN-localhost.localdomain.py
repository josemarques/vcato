 # -*- coding: utf-8 -*-
#!/usr/bin/env python
#!/usr/bin/python -u

#Transfer function analysis
import sys, getopt

import control
import matplotlib.pyplot as plt

import sympy
import cmath
import math

import IAMP
import ICsE

def FCV(values):#Find complex values
	COMPV=[]
	for i in range(len(values)):
		if values[i].imag!=0:
			if(values[i].real!=0):
				# print("Complex value: ",values[i])
				COMPV.append(values[i])
	return COMPV

def POLE_ANLS(tf):#Pole analysis of a transfer function tf, resonant frequency and damping factor

# import control
# import sympy
	"""
	En el caso de polos o ceros complejos, de la forma general
(s2+as+b)
, los parámetros de estos son, según la formula general,
(s2+2𝜉𝜔ns+𝜔n2)
.

Siendo
𝜔n
 la frecuencia para la que el polo o cero actúa, y
𝜉
 ,coeficiente de amortiguamiento, de forma que el efecto del polo o cero complejo se ve incrementado si
𝜉→0
. Una aproximación de la sobreoscilación provocada por los polos es

Gj𝜔𝜔=𝜔n=1/2𝜉

	"""
	x=sympy.symbols("x")

	p = control.pole(tf)
	SCV=FCV(p)
	#print("SCV=",SCV)
	if len(SCV)>1:
		OSCGF=sympy.expand((x-SCV[0])*(x-SCV[1]))
		#print("OSCGF=",OSCGF)
		OSCGF=sympy.poly(OSCGF)
		#print("OSCGF=",OSCGF)

		OSCGR_COEF=OSCGF.all_coeffs()
		#print("OSCGR_COEF=",OSCGR_COEF)
		RaFRQ=(OSCGR_COEF[2]**0.5)#Resonant angular frequency
		RFRQ=((RaFRQ)/(2*math.pi))#Resonant frequency
		print("RFRQ [Hz]= ",RFRQ)

		DMPF=(OSCGR_COEF[1]/(RaFRQ*2))#Damping factor
		print("DMPF [arb.units]= ",DMPF)

		return(RaFRQ, RFRQ, DMPF)

	else:
		print("NO COMPLEX ROOTS")
		return(0, 0, 0)
		#raise Exception("Sorry, no numbers below zero")


def aTF_ANLS(tf):#Analytic transfer function analisys
	s=sympy.Symbol("s")
	zeroes,poles=sympy.fraction(tf)
	#zeroes=sympy.expand(zeroes)
	#poles=sympy.expand(poles)
	zeroes=sympy.factor(zeroes)
	poles=sympy.factor(poles)
	print("zeroes",zeroes)
	print("poles",poles)
	print("aE_ANLS")
	print(sympy.roots(zeroes,s))

def TEST(argv):
	return 0

if __name__ == "__main__":
	TEST(sys.argv[1:])
