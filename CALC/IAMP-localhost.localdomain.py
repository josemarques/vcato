 # -*- coding: utf-8 -*-
#!/usr/bin/env python
#!/usr/bin/python -u

#Inverter RF amplifier

import sys, getopt

import control
import matplotlib.pyplot as plt

import sympy
import math

import TFAN

#Funcion definition
def IAwD_tFaC():#Inverter amplyfier with delay transfer function analytic calculation

	s=sympy.Symbol("s")
	R_d=sympy.Symbol("R_d",real=True)
	C_d=sympy.Symbol("C_d",real=True)
	g_m=sympy.Symbol("g_m",real=True)

	V_IN=sympy.Symbol("V_IN")
	#Performance Analysis of Voltage Controlled Ring Oscillators (doi:10.1007/978-981-10-0755-2_4) "Errata on model"
	AMP=-g_m
	Z_Cd=1/(C_d*s)
	I=V_IN/(R_d+Z_Cd)
	V_OUT=I*Z_Cd

	IAwD=sympy.factor(V_OUT/V_IN)

	#IAwD=(-g_m*R_d)/(1+s*R_d*C_d)
	print("IAwD=",IAwD)
	return IAwD

def CC_tFaC():#Capacitive coupling transfer function analytic calculation

	s=sympy.Symbol("s")
	C_cc=sympy.Symbol("C_cc",real=True)
	R_g=sympy.Symbol("R_g",real=True)

	V_IN=sympy.Symbol("V_IN")
	Z_Ccc=1/(C_cc*s)
	Z_Rg=R_g
	I=V_IN/(Z_Ccc+Z_Rg)
	V_OUT=I*Z_Rg

	CC=sympy.factor((V_OUT/V_IN))
	print("CC=",CC)

	return CC

def IAwD_CC_tFaC():#Inverter amplyfier with delay and capacitive coupling transfer fuction calculation analitic calculation
	return(IAwD_tFaC()*CC_tFaC())

def IAwD_tfC(g_m,r_d,c_d):#Inverter amplyfier with delay transfer function calculation

	IAwDn=[-r_d*g_m]
	IAwDd=[c_d*r_d,1]
	IAwD = control.tf(IAwDn,IAwDd)
	#print("IAwD(s)=",IAwD)

	return IAwD

def CC_tfC(c_cc,r_g): # Capacitive coupling transfer function calculation

	CCn=[c_cc*r_g,0]
	CCd=[c_cc*r_g,1]
	CC=control.tf(CCn,CCd)
	#print("CC(s)=",CC)

	return(CC)


def IAwD_CC_tfC(g_m,r_d,c_d,c_cc,r_g):#Inverter amplyfier with delay and capacitive coupling transfer fuction calculation
	IAwD_CC=control.series(IAwD_tfC(g_m,r_d,c_d),CC_tfC(c_cc,r_g))
	#print("IAwD_CC(s)=",IAwD_CC)
	return(IAwD_CC)

#System variables
g_m=1.0000
R_d=1
C_d=1*10**-12
R_g=1000
C_cc=1*10**-9

TSTP=1*10**(-8)

def TEST(argv):
	IAwD_tFaC()
	CC_tFaC()



	#IAwD_CC=IAwD*CC
	IAwD_CC=IAwD_CC_tfC(g_m,R_d,C_d,C_cc,R_g)
	print("IAwD_CC(s)=",IAwD_CC)

	#Analysis
	z = control.zero(IAwD_CC)
	p = control.pole(IAwD_CC)
	print("Zeroes ")
	print(z)
	print("Poles ")
	print(p)

	print("dcgain= ",control.dcgain(IAwD_CC))

	ASTBOSC=(IAwD_CC/(1-(IAwD_CC*IAwD_CC)))

	print("TFAN.POLE_ANLS(ASTBOSC)", TFAN.POLE_ANLS(ASTBOSC))

	control.bode_plot(IAwD_CC)
	control.bode_plot(ASTBOSC)
	plt.grid()
	plt.show()

	print("TFAN.POLE_ANLS(ASTBOSC)", TFAN.POLE_ANLS(ASTBOSC))

	t, y = control.impulse_response(ASTBOSC,TSTP)
	#t,y=control.step_response(ASTBOSC,TSTP)
	print("y",y)
	print("t",t)
	t= t[1:]#Remove initial point
	y= y[1:]#Remove initial point
	plt.plot(t, y)
	plt.title("Step Response")
	plt.xlabel("t")
	plt.ylabel("y")
	plt.grid()
	plt.show()

	#control.nyquist_plot(CS)

if __name__ == "__main__":
	TEST(sys.argv[1:])
