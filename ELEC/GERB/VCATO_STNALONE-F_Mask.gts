G04 #@! TF.GenerationSoftware,KiCad,Pcbnew,7.0.9*
G04 #@! TF.CreationDate,2023-12-13T08:52:50+01:00*
G04 #@! TF.ProjectId,VCATO_STNALONE,56434154-4f5f-4535-944e-414c4f4e452e,rev?*
G04 #@! TF.SameCoordinates,Original*
G04 #@! TF.FileFunction,Soldermask,Top*
G04 #@! TF.FilePolarity,Negative*
%FSLAX46Y46*%
G04 Gerber Fmt 4.6, Leading zero omitted, Abs format (unit mm)*
G04 Created by KiCad (PCBNEW 7.0.9) date 2023-12-13 08:52:50*
%MOMM*%
%LPD*%
G01*
G04 APERTURE LIST*
G04 Aperture macros list*
%AMRoundRect*
0 Rectangle with rounded corners*
0 $1 Rounding radius*
0 $2 $3 $4 $5 $6 $7 $8 $9 X,Y pos of 4 corners*
0 Add a 4 corners polygon primitive as box body*
4,1,4,$2,$3,$4,$5,$6,$7,$8,$9,$2,$3,0*
0 Add four circle primitives for the rounded corners*
1,1,$1+$1,$2,$3*
1,1,$1+$1,$4,$5*
1,1,$1+$1,$6,$7*
1,1,$1+$1,$8,$9*
0 Add four rect primitives between the rounded corners*
20,1,$1+$1,$2,$3,$4,$5,0*
20,1,$1+$1,$4,$5,$6,$7,0*
20,1,$1+$1,$6,$7,$8,$9,0*
20,1,$1+$1,$8,$9,$2,$3,0*%
G04 Aperture macros list end*
%ADD10C,0.718420*%
%ADD11C,1.600000*%
%ADD12RoundRect,0.140000X0.170000X-0.140000X0.170000X0.140000X-0.170000X0.140000X-0.170000X-0.140000X0*%
%ADD13R,1.700000X1.700000*%
%ADD14O,1.700000X1.700000*%
%ADD15R,1.500000X5.080000*%
G04 APERTURE END LIST*
D10*
X103790000Y-76916000D03*
D11*
X103790000Y-75900000D03*
D10*
X103790000Y-74884000D03*
X98710000Y-76916000D03*
D11*
X98710000Y-75900000D03*
D10*
X98710000Y-74884000D03*
D12*
X101250000Y-69580000D03*
X101250000Y-68620000D03*
D13*
X108650000Y-63660000D03*
D14*
X108650000Y-66200000D03*
D13*
X93850000Y-63660000D03*
D14*
X93850000Y-66200000D03*
D15*
X101250000Y-64460000D03*
X97000000Y-64460000D03*
X105500000Y-64460000D03*
M02*
