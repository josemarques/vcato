 # -*- coding: utf-8 -*-
#!/usr/bin/env python
#!/usr/bin/python -u

import math
import cmath

#Varactor resonant tank tuning

class VCVC:#Voltage controlled varactor circuit
	def __init__(self, c=0, v_vc=0):#init with given variables
		self.init(c, v_vc)

	def init(self, c=0, v_vc=0):#initialization routine
		if(c==0):
			c=self.SVCC([1.575*10**(-12),0.97*10**(-12),0.61*10**(-12)],[1,3,6])#Varactor SMV1231-079LF

		assert(len(c)==3), "The parameters of the varactor are 3: [c0, c1, c2]"

		self.c=list(c)#Varactor 0 parameters
		self.c0=list(c)#Varactor 0 parameters
		self.c1=list(c)#Varactor 1 parameters

		self.v_vc=v_vc#Varactor mean reverse voltage

		self.VPC()#Varactor capacitance calculation

	def SVCC(self,caps,v_vcs):#single Varactor capacitance calibration. Cuadratic fit
		assert len(caps)==3 and len(v_vcs)==3, "Len of capacitances and voltages needs to be == 3"
		c=[]
		c.append((caps[0]*v_vcs[1]**2*v_vcs[2] - caps[0]*v_vcs[1]*v_vcs[2]**2 - caps[1]*v_vcs[0]**2*v_vcs[2] + caps[1]*v_vcs[0]*v_vcs[2]**2 + caps[2]*v_vcs[0]**2*v_vcs[1] - caps[2]*v_vcs[0]*v_vcs[1]**2)/(v_vcs[0]**2*v_vcs[1] - v_vcs[0]**2*v_vcs[2] - v_vcs[0]*v_vcs[1]**2 + v_vcs[0]*v_vcs[2]**2 + v_vcs[1]**2*v_vcs[2] - v_vcs[1]*v_vcs[2]**2))

		c.append((-caps[0]*v_vcs[1]**2 + caps[0]*v_vcs[2]**2 + caps[1]*v_vcs[0]**2 - caps[1]*v_vcs[2]**2 - caps[2]*v_vcs[0]**2 + caps[2]*v_vcs[1]**2)/(v_vcs[0]**2*v_vcs[1] - v_vcs[0]**2*v_vcs[2] - v_vcs[0]*v_vcs[1]**2 + v_vcs[0]*v_vcs[2]**2 + v_vcs[1]**2*v_vcs[2] - v_vcs[1]*v_vcs[2]**2))

		c.append((caps[0]*v_vcs[1] - caps[0]*v_vcs[2] - caps[1]*v_vcs[0] + caps[1]*v_vcs[2] + caps[2]*v_vcs[0] - caps[2]*v_vcs[1])/(v_vcs[0]**2*v_vcs[1] - v_vcs[0]**2*v_vcs[2] - v_vcs[0]*v_vcs[1]**2 + v_vcs[0]*v_vcs[2]**2 + v_vcs[1]**2*v_vcs[2] - v_vcs[1]*v_vcs[2]**2))

		return c

	def VPC(self):#Varactor pair capacitance
		assert(len(self.c0)==3), "The parameters of the varactor need to be 3: [c0, c1, c2]"
		assert(len(self.c1)==3), "The parameters of the varactor need to be 3: [c0, c1, c2]"
		vc=((1/(self.c0[0]+self.c0[1]*self.v_vc+self.c0[2]*self.v_vc**2))+(1/(self.c1[0]+self.c1[1]*self.v_vc+self.c1[2]*self.v_vc**2)))**(-1)

		self.VC=vc

		return vc

	def ZVPC(self,omega):#varactor pair capacitance impedance

		self.omega=omega
		self.Z_VPC=1/(complex(0,self.omega*self.VC))

		return self.Z_VPC




def TEST(argv):
	VARPAIR=VCVC()
	print("\nVCVC.c0=",VARPAIR.c0,"\n")
	print("\nVCVC.Z_VPC=",VARPAIR.ZVPC(100*(10**6)*2*math.pi),"\n")
	return 0

if __name__ == "__main__":
	TEST("""sys.argv[1:]""")
