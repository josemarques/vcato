 # -*- coding: utf-8 -*-
#!/usr/bin/env python
#!/usr/bin/python -u

#Voltage controlled astable tuned oscillator

import sys, getopt

import control
import matplotlib.pyplot as plt

import sympy
import cmath
import math

import sys
#sys.path.append('VCATO/SOFT/')
import VCATO

#Funcion definition
def aCs():#analitic calculations
	omega=sympy.Symbol("omega",real=True)

	gm=sympy.Symbol("gm",real=True)
	rd=sympy.Symbol("rd",real=True)
	cd=sympy.Symbol("cd",real=True)
	rg=sympy.Symbol("rg",real=True)
	ccc=sympy.Symbol("ccc",real=True)


	ir=sympy.Symbol("ir",real=True)

	lt=sympy.Symbol("lt",real=True)
	mur=sympy.Symbol("mur",real=True)
	mui=sympy.Symbol("mui",real=True)
	mu=mur-mui*sympy.I

	ct=sympy.Symbol("ct",real=True)
	epsilonr=sympy.Symbol("epsilonr",real=True)
	epsiloni=sympy.Symbol("epsiloni",real=True)
	epsilon=epsilonr-epsiloni*sympy.I
	c0=sympy.Symbol("c0",real=True)
	c1=sympy.Symbol("c1",real=True)
	c2=sympy.Symbol("c2",real=True)
	c=list([c0,c1,c2])

	v_vc=sympy.Symbol("v_vc",real=True)

	ro=sympy.Symbol("ro",real=True)

	ao=VCATO.AO(omega, gm, rd, cd, ccc, rg)
	vcato=VCATO.VCATO( omega, gm, rd, cd, ccc, rg, ro, ir, lt, mur, mui, ct, epsilonr, epsiloni, c, v_vc)

	#Analitic basig impedances calculation

	ao.CCIDA.Z_RD=ao.CCIDA.RD

	ao.CCIDA.Z_CD=1/(sympy.I*ao.CCIDA.omega*ao.CCIDA.CD)

	ao.CCIDA.Z_CC=1/(sympy.I*ao.CCIDA.omega*ao.CCIDA.CC)

	ao.CCIDA.Z_RG=ao.CCIDA.RG

	ao.TF()

	vcato.RTNK.mu=mu
	vcato.RTNK.epsilon=epsilon
	vcato.RTNK.Z_LPT=sympy.I*vcato.RTNK.omega*vcato.RTNK.LPT*vcato.RTNK.mu
	#print("\nself.Z_LPT=",self.Z_LPT,"\n")
	vcato.RTNK.Z_IR=vcato.RTNK.IR
	#print("\nself.Z_IR=",self.Z_IR,"\n")
	vcato.RTNK.Z_CPT=1/(sympy.I*vcato.RTNK.omega*vcato.RTNK.CPT*vcato.RTNK.epsilon)
	#print("\nself.Z_CPT=",self.Z_CPT,"\n")
	vcato.RTNK.Z_CVPC=1/(sympy.I*vcato.RTNK.VP.VPC())

	vcato.CCIDA.Z_RD=vcato.CCIDA.RD

	vcato.CCIDA.Z_CD=1/(sympy.I*vcato.CCIDA.omega*vcato.CCIDA.CD)

	vcato.CCIDA.Z_CC=1/(sympy.I*vcato.CCIDA.omega*vcato.CCIDA.CC)

	vcato.CCIDA.Z_RG=vcato.CCIDA.RG

	vcato.TF()


	return(ao,vcato)

def Z_PICRT_aC():#PICRT impedance analitic calculation

	VCORT=Z_PICRT_aCI()

	Z_PICRT=VCORT.IMP()

	return(Z_PICRT)

def SRF_PICRT_aC():#PICRT self resonant frequency analitic calculation
	IM_Z_PICRT=sympy.factor(sympy.im(Z_PICRT_aC()))
	#print("\n","IM_Z_PICRT=",IM_Z_PICRT,"\n")

	#omega=sympy.Symbol("omega",real=True)
	#SRF=sympy.solve(IM_Z_PICRT,omega, simplify=False, manual=True,rational=False)
	#print("\n","SRF=",SRF,"\n")

	VCORT=Z_PICRT_aCI()

	VCORT.mur=sympy.Symbol("mur",real=True)
	VCORT.epsilonr=sympy.Symbol("epsilonr",real=True)

	SRF_PICRT=sympy.factor(VCORT.SRF())

	return(SRF_PICRT)

#System variables
g_m=1.0000436536573636
R_d=1
C_d=1*10**-14
R_g=10000
C_cc=1*10**-8
R_S=1
L_S0=46*10**-9
mur=1
L_S=L_S0*mur
epr=1
C_S0=2.5*10**-12
C_SV=1.5*10**-12
C_S=C_S0*epr+C_SV
R_o=1

TSTP=1*10**(-7)

#
# g_m=1.1
# R_d=1
# C_d=1*10**-4
# R_g=10000
# C_cc=1*10**4
# R_S=1
# L_S0=1*10**1
# mur=1
# L_S=L_S0*mur
# epr=1
# C_S0=2.5*10**1
# C_SV=1.5*10**1
# C_S=C_S0*epr+C_SV
# R_o=1
#
# TSTP=1000


def main(argv):
	omega=sympy.Symbol("omega",real=True)


	ao,vcato=aCs()

	print("\nAO.TF=",ao.TF,"\n")
	print("\nsympy.Abs(ao.TF)=",sympy.Abs(ao.TF),"\n")
	difao=sympy.diff(sympy.Abs(ao.TF),omega)
	print("\ndifao=",difao,"\n")


	print("\nvcato.TF=",vcato.TF,"\n")
	print("\nsympy.Abs(vcato.TF)=",sympy.Abs(vcato.TF),"\n")
	difvcato=sympy.diff(sympy.Abs(vcato.TF),omega)
	print("\ndifvcato=",difvcato,"\n")


	print("\nsympy.solve(difao,omega)=",sympy.solve(difao,omega),"\n")

	print("\nsympy.solve(difao,omega)=",sympy.solve(difvcato,omega),"\n")



if __name__ == "__main__":
	main(sys.argv[1:])
