 # -*- coding: utf-8 -*-
#!/usr/bin/env python
#!/usr/bin/python -u

import math
import cmath

#Capacitive coupled inverter delayed amplifier

class CCIDA:#Capacitive coupled inverter delayed amplifier
	def init(self,omega, gm, rd, cd, ccc, rg):

		self.f=omega/(2*math.pi)
		self.omega=omega

		self.GM=gm#amplifier gain

		self.RD=rd#Delay resistance
		self.CD=cd#Capacitance delay
		self.CC=ccc#Capacitance of capacitive coupling
		self.RG=rg#Output ground resistance

	def __init__(self,omega=2*math.pi*100*10**6, gm=1, rd=1, cd=0.1*10**(-12), ccc=1*10**(-9), rg=470):
		self.init(omega, gm, rd, cd, ccc, rg)

	def sIMP(self, omega):#Sub impedance calculations
		self.omega=omega

		self.Z_RD=self.RD

		self.Z_CD=1/(complex(0,self.omega*self.CD))

		self.Z_CC=1/(complex(0,self.omega*self.CC))

		self.Z_RG=self.RG

	def TF(self):#transfer function
		#Inverter amp TF
		IATF=-self.GM

		#Amplifier delay TF
		ADTF=self.Z_CD/(self.Z_CD+self.Z_RD)

		#Capacitive coupling TF
		CCTF=(self.Z_RG)/(self.Z_RG+self.Z_CC)

		self.TF=IATF*ADTF*CCTF

		return(self.TF)



def TEST(argv):
	CCIDAo=CCIDA(omega=2*math.pi*100*10**6, gm=1, rd=1, cd=0.1*10**(-12), ccc=1*10**(-9), rg=470)

	CCIDAo.sIMP(2*math.pi*100*10**6)
	print("\nCCIDAo.TF()=",CCIDAo.TF(),"\n")

	return 0

if __name__ == "__main__":
	TEST("""sys.argv[1:]""")
