#ifndef VCATO_H_INCLUDED
#define VCATO_H_INCLUDED

//Voltage controlled astable tuned oscillator

#include <algorithm>    // std::sort
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <math.h>
#include <string>
#include <functional>
#include <limits>

#include <iostream>


#include "../SUBM/ICsE/SOFT/ICsE.h"

class VCATO{//Voltage contolled astable tuned oscillator
	public:

	ICTsE TNK;//Resonant tank

	double VCC;//Voltage supply
	double I;//Current supply

	double LL, RL;
	double CD, RD;
	double gm;

	VCATO();
	VCATO(const VCATO& VCATO);//Duplicador de clase
	VCATO& operator=(const VCATO& vcato);//Igualador de clase

};

void print(VCATO vcato);

void print(std::vector<VCATO> vcato);

void print(std::vector<std::vector<VCATO>> vcato);

#endif
