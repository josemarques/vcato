#include "VCATO.h"

VCATO::VCATO(){//Constructor
}

VCATO::VCATO(const VCATO& vcato){
	TNK=vcato.TNK;//Resonant tank

	VCC=vcato.VCC;//Voltage supply
	I=vcato.I;//Current supply

	LL=vcato.LL;
	RL=vcato.RL;
	CD=vcato.CD;
	RD=vcato.RD;
	gm=vcato.gm;
}

VCATO& VCATO::operator=(const VCATO& vcato){

	TNK=vcato.TNK;//Resonant tank

	VCC=vcato.VCC;//Voltage supply
	I=vcato.I;//Current supply

	LL=vcato.LL;
	RL=vcato.RL;
	CD=vcato.CD;
	RD=vcato.RD;
	gm=vcato.gm;

	return *this;
} //Igualador de clase

void print(VCATO vcato){
			std::cout<<"VCC:"<<vcato.VCC;
		std::cout<<"	TNK.F:"<<vcato.TNK.F;
		std::cout<<"	TNK.W:"<<vcato.TNK.W;
		std::cout<<"	TNK.H:"<<vcato.TNK.H;
		std::cout<<"	TNK.epsilon_eff:"<<vcato.TNK.epsilon_eff;
		std::cout<<"	TNK.mu_eff:"<<vcato.TNK.mu_eff;
		std::cout<<"	TNK.rf_m_eff:"<<vcato.TNK.rf_m_eff;
		std::cout<<"	TNK.rf_e_eff:"<<vcato.TNK.rf_e_eff;
		//std::cout<<std::endl;
}

void print(std::vector<VCATO> vcato){
	for(int i=0;i<vcato.size();i++){
		std::cout<<"VCATO["<<i<<"]"<<std::endl;
		print(vcato[i]);
		std::cout<<std::endl;
	}
	std::cout<<std::endl;
}

void print(std::vector<std::vector<VCATO>> vcato){
	for(int i=0;i<vcato.size();i++){
		std::cout<<"VCATO["<<i<<"][]"<<std::endl;
		print(vcato[i]);

	}
	std::cout<<std::endl;
}

