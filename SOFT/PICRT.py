 # -*- coding: utf-8 -*-
#!/usr/bin/env python
#!/usr/bin/python -u

import math
import cmath

import VRTT

#Parallel Inducto capacitive resonant tank. With varactor control

class PICRT:#Parallel Inducto capacitive resonant tank
	def init(self, omega, ro, ir, lt, mur, mui, ct, epsilonr, epsiloni, c, v_vc):

		self.VP=VRTT.VCVC(c, v_vc)#Varactor pair for voltage control oscilation

		self.f=omega/(2*math.pi)
		self.omega=omega

		self.RO=ro

		self.IR=ir#Inductance resisitance

		self.LPT=lt#Tank phisical autoinductance
		self.mur=mur
		self.mui=mui

		self.CPT=ct#Tank phisical capacitance
		self.epsilonr=epsilonr
		self.epsiloni=epsiloni


	def __init__(self, omega=(2*math.pi)*100*10**6, ro=1, ir=0, lt=35*10**(-9), mur=1, mui=0, ct=2*10**(-12), epsilonr=1, epsiloni=0, c=[0,0,0], v_vc=0):

		self.init(omega, ro, ir, lt, mur, mui, ct, epsilonr, epsiloni, c, v_vc)

	def sIMP(self, omega):#Sub Impedance calculation
		self.omega=omega
		#print("\nself.omega=",self.omega,"\n")
		self.mu=self.mur+complex(0,-self.mui)#mu complex= mur -mui * i
		self.epsilon=epsilonr+complex(0,-epsiloni)#epsilon complex= epsilon -epsilon * i

		self.Z_LPT=complex(0,self.omega*self.LPT*self.mu)
		#print("\nself.Z_LPT=",self.Z_LPT,"\n")

		self.Z_IR=self.IR
		#print("\nself.Z_IR=",self.Z_IR,"\n")

		self.Z_CPT=1/(complex(0,self.omega*self.CPT*self.epsilon))
		#print("\nself.Z_CPT=",self.Z_CPT,"\n")

		self.Z_CVPC=self.VP.ZVPC(self.omega)
		#print("\nself.Z_CVPC=",self.Z_CVPC,"\n")


	def IMP(self):#Impedance calculation
		#intermidiate circ

		P_ZCPT_ZVPC=1/((1/(self.Z_CPT))+(1/(self.Z_CVPC)))#Paralel varactor pair and capacitance
		#print("\nP_ZCPT_ZVPC=",P_ZCPT_ZVPC,"\n")

		S_ZIR_ZLPT=self.Z_LPT+self.Z_IR#Series resitance and inductance
		#print("\nS_ZIR_ZLPT=",S_ZIR_ZLPT,"\n")

		#Final impedance

		self.Z_PICRT=1/((1/(S_ZIR_ZLPT))+(1/(P_ZCPT_ZVPC)))#Paralel final combination


		return(self.Z_PICRT)

	def SRF(self):#Self resonant frequency calculation
		#Maxima solution
		#self.ARF=((((-(self.VP.c[2]**2*self.IR**2*self.mur**2*self.VP.v_vc**4)-2*self.VP.c[1]*self.VP.c[2]*self.IR**2*self.mur**2*self.VP.v_vc**3+(2*self.VP.c[2]*self.LPT*self.mur**3+(-(4*self.VP.c[2]*self.CPT*self.epsilonr)-2*self.VP.c[0]*self.VP.c[2]-self.VP.c[1]**2)*self.IR**2*self.mur**2+2*self.VP.c[2]*self.LPT*self.mui**2*self.mur)*self.VP.v_vc**2+(2*self.VP.c[1]*self.LPT*self.mur**3+(-(4*self.VP.c[1]*self.CPT*self.epsilonr)-2*self.VP.c[0]*self.VP.c[1])*self.IR**2*self.mur**2+2*self.VP.c[1]*self.LPT*self.mui**2*self.mur)*self.VP.v_vc+(4*self.CPT*self.epsilonr+2*self.VP.c[0])*self.LPT*self.mur**3+(-(4*self.CPT**2*self.epsilonr**2)-4*self.VP.c[0]*self.CPT*self.epsilonr-self.VP.c[0]**2)*self.IR**2*self.mur**2+(4*self.CPT*self.epsilonr+2*self.VP.c[0])*self.LPT*self.mui**2*self.mur)**(1/2))+self.VP.c[2]*self.IR*self.mui*self.VP.v_vc**2+self.VP.c[1]*self.IR*self.mui*self.VP.v_vc+(2*self.CPT*self.epsilonr+self.VP.c[0])*self.IR*self.mui)/((self.VP.c[2]*self.LPT*self.mur**2+self.VP.c[2]*self.LPT*self.mui**2)*self.VP.v_vc**2+(self.VP.c[1]*self.LPT*self.mur**2+self.VP.c[1]*self.LPT*self.mui**2)*self.VP.v_vc+(2*self.CPT*self.epsilonr+self.VP.c[0])*self.LPT*self.mur**2+(2*self.CPT*self.epsilonr+self.VP.c[0])*self.LPT*self.mui**2))

		#sympy solution
		self.RF=(-self.IR*self.mui*(self.VP.c[0]*self.mui**2 + self.VP.c[0]*self.mur**2 + self.VP.c[1]*self.mui**2*self.VP.v_vc + self.VP.c[1]*self.mur**2*self.VP.v_vc + self.VP.c[2]*self.mui**2*self.VP.v_vc**2 + self.VP.c[2]*self.mur**2*self.VP.v_vc**2 + 2*self.CPT*self.epsilonr*self.mui**2 + 2*self.CPT*self.epsilonr*self.mur**2) + ((-self.mur*(self.VP.c[0] + self.VP.c[1]*self.VP.v_vc + self.VP.c[2]*self.VP.v_vc**2 + 2*self.CPT*self.epsilonr)*(self.VP.c[0]*self.IR**2*self.mur + self.VP.c[1]*self.IR**2*self.mur*self.VP.v_vc + self.VP.c[2]*self.IR**2*self.mur*self.VP.v_vc**2 + 2*self.CPT*self.epsilonr*self.IR**2*self.mur - 2*self.LPT*self.mui**2 - 2*self.LPT*self.mur**2))**(1/2))*(-self.mui**2 - self.mur**2))/(self.LPT*(self.mui**2 + self.mur**2)*(self.VP.c[0]*self.mui**2 + self.VP.c[0]*self.mur**2 + self.VP.c[1]*self.mui**2*self.VP.v_vc + self.VP.c[1]*self.mur**2*self.VP.v_vc + self.VP.c[2]*self.mui**2*self.VP.v_vc**2 + self.VP.c[2]*self.mur**2*self.VP.v_vc**2 + 2*self.CPT*self.epsilonr*self.mui**2 + 2*self.CPT*self.epsilonr*self.mur**2)), (-self.IR*self.mui*(self.VP.c[0]*self.mui**2 + self.VP.c[0]*self.mur**2 + self.VP.c[1]*self.mui**2*self.VP.v_vc + self.VP.c[1]*self.mur**2*self.VP.v_vc + self.VP.c[2]*self.mui**2*self.VP.v_vc**2 + self.VP.c[2]*self.mur**2*self.VP.v_vc**2 + 2*self.CPT*self.epsilonr*self.mui**2 + 2*self.CPT*self.epsilonr*self.mur**2) + ((-self.mur*(self.VP.c[0] + self.VP.c[1]*self.VP.v_vc + self.VP.c[2]*self.VP.v_vc**2 + 2*self.CPT*self.epsilonr)*(self.VP.c[0]*self.IR**2*self.mur + self.VP.c[1]*self.IR**2*self.mur*self.VP.v_vc + self.VP.c[2]*self.IR**2*self.mur*self.VP.v_vc**2 + 2*self.CPT*self.epsilonr*self.IR**2*self.mur - 2*self.LPT*self.mui**2 - 2*self.LPT*self.mur**2))**(1/2))*(self.mui**2 + self.mur**2))/(self.LPT*(self.mui**2 + self.mur**2)*(self.VP.c[0]*self.mui**2 + self.VP.c[0]*self.mur**2 + self.VP.c[1]*self.mui**2*self.VP.v_vc + self.VP.c[1]*self.mur**2*self.VP.v_vc + self.VP.c[2]*self.mui**2*self.VP.v_vc**2 + self.VP.c[2]*self.mur**2*self.VP.v_vc**2 + 2*self.CPT*self.epsilonr*self.mui**2 + 2*self.CPT*self.epsilonr*self.mur**2))

		#self.RF=self.RF/(2*math.pi)

		return self.RF

	def TF(self):#transfer function
		self.IMP()

		self.Z_RO=self.RO
		self.TF=self.Z_RO/(self.Z_PICRT+self.Z_RO)
		return 0


def TEST(argv):
	CIC=PICRT(f=2*math.pi*100*10**6, ir=1, lt=35*10**(-9), mur=1, mui=0, ct=2*10**(-12), epsilonr=1, epsiloni=0, c=0, v_vc=0)

	CIC.sIMP(2*math.pi*100*10**6)
	print("\nCIC.IMP=",CIC.IMP(),"\n")

	print("\nCIC.SRF=",CIC.SRF(),"\n")

	return 0

if __name__ == "__main__":
	TEST("""sys.argv[1:]""")
