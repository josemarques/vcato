 # -*- coding: utf-8 -*-
#!/usr/bin/env python
#!/usr/bin/python -u

import math
import cmath

sys.path.append('SUBM/MATH/PY')
import NMMT

import VCATO

class VCATOSET:#Set of VCATO states
	def init(self, vcatos):

		self.VCATOS=list(vcatos)

	def __init__(self, vcatos):

		self.init(vcatos)

#vcato set data exchanges

#vcato set procesing routines
def MPR(vcatoset):#Measurement processing. vcatoset contains 2 measurmenets separated epsilon vatractor control voltage
	RF0=vcatoset.VCATOS[0].RF
	fRF0=RF0**(-2)
	V_VC0=vcatoset.VCATOS[0].RTNK.VP.v_vc

	RF1=vcatoset.VCATOS[1].RF
	fRF1=RF1**(-2)
	V_VC1=vcatoset.VCATOS[1].RTNK.VP.v_vc

	MDER=NMMT.DRV(fRF0, V_VC0, fRF1, V_VC1)
	M=(fRF0+fRF1)/2

	return(M,MDER)

def CM(vcatosetm, vcatosetc):#calibrated measurements
	M,MD=MPR(vcatosetm)

	C,CD=MPR(vcatosetc)

	fmu=MD/CD#fmu=delt*mur
	fepsilon=(M/fmu)-C#fepsilon=C_S0*L_S0*(delt*epr - 1)

	return fmi, fepsilon

def RM((fmuu,fepsilonu),(fmik,fepsilonk),(muk=1,epsilonk=1)):#Referenced measurement. fmuk= known mu function, muk=known mu, fmuu= mu function unknown ,etc...
	delta=fmik/muk
	LS0CS0=fepsilonk/(delta*epsilonk - 1)

	mu=fmuu*muk/fmik
	epsilon=(fepsilonu*fmik*epsilonk - fepsilonu*muk + fepsilonk*muk)/(fepsilonk*fmik)

	return mu, epsilon




def TEST(argv):
	CIC=PICRT(f=2*math.pi*100*10**6, ir=1, lt=35*10**(-9), mur=1, mui=0, ct=2*10**(-12), epsilonr=1, epsiloni=0, c=0, v_vc=0)

	CIC.sIMP(2*math.pi*100*10**6)
	print("\nCIC.IMP=",CIC.IMP(),"\n")

	print("\nCIC.SRF=",CIC.SRF(),"\n")

	return 0

if __name__ == "__main__":
	TEST("""sys.argv[1:]""")
