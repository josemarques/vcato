 # -*- coding: utf-8 -*-
#!/usr/bin/env python
#!/usr/bin/python -u

import math
import cmath

import PICRT
import CCIDA

#Parallel Inducto capacitive resonant tank. With varactor control
class AO:#Astable oscillator
	def init(self, omega, gm, rd, cd, ccc, rg):

		self.CCIDA=CCIDA.CCIDA(omega, gm, rd, cd, ccc, rg)

		self.f=omega/(2*math.pi)
		self.omega=omega

		self.RF=0
		self.PH=0
		self.PW=0


	def __init__(self, omega=(2*math.pi)*100*10**6, gm=1, rd=1, cd=0.1*10**(-12), ccc=1*10**(-9), rg=470):
		self.init(omega, gm, rd, cd, ccc, rg)

	def sIMP(self,omega):#sub impedances calculation
		self.f=omega/(2*math.pi)
		self.omega=omega
		self.CCIDA.sIMP(self.omega)
		self.RTNK.sIMP(self.omega)


	def TF(self):#Transfer function calculation

		self.CCIDA.TF()
		self.TF=(self.CCIDA.TF)/(1-(self.CCIDA.TF*self.CCIDA.TF))#Astable oscillator transfer function. Without tuning tank

		return self.TF

	def RF(self):
		#Maxima solutions: [omega=-(1/(sqrt(ccc)·sqrt(cd)·sqrt(rd)·sqrt(rg))),omega=1/(sqrt(ccc)·sqrt(cd)·sqrt(rd)·sqrt(rg)),omega=-(sqrt(sqrt(-((2·ccc^5·gm^3·rg^5)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2))+(8·ccc^5·gm·rg^5)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+(16·ccc^4·cd·gm·rd·rg^4)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(2·ccc^3·cd^2·gm^3·rd^2·rg^3)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+(16·ccc^3·cd^2·gm·rd^2·rg^3)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+(16·ccc^2·cd^3·gm·rd^3·rg^2)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+(8·ccc·cd^4·gm·rd^4·rg)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+ccc^4·gm^4·rg^4-4·ccc^4·gm^2·rg^4+ccc^4·rg^4-8·ccc^3·cd·gm^2·rd·rg^3-4·ccc^2·cd^2·gm^2·rd^2·rg^2-2·ccc^2·cd^2·rd^2·rg^2+cd^4·rd^4)+ccc·gm·rg·sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-ccc^2·rg^2-cd^2·rd^2)/(sqrt(2)·ccc·cd·rd·rg)),omega=sqrt(sqrt(-((2·ccc^5·gm^3·rg^5)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2))+(8·ccc^5·gm·rg^5)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+(16·ccc^4·cd·gm·rd·rg^4)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(2·ccc^3·cd^2·gm^3·rd^2·rg^3)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+(16·ccc^3·cd^2·gm·rd^2·rg^3)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+(16·ccc^2·cd^3·gm·rd^3·rg^2)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+(8·ccc·cd^4·gm·rd^4·rg)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+ccc^4·gm^4·rg^4-4·ccc^4·gm^2·rg^4+ccc^4·rg^4-8·ccc^3·cd·gm^2·rd·rg^3-4·ccc^2·cd^2·gm^2·rd^2·rg^2-2·ccc^2·cd^2·rd^2·rg^2+cd^4·rd^4)+ccc·gm·rg·sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-ccc^2·rg^2-cd^2·rd^2)/(sqrt(2)·ccc·cd·rd·rg),omega=-(sqrt(-sqrt(-((2·ccc^5·gm^3·rg^5)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2))+(8·ccc^5·gm·rg^5)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+(16·ccc^4·cd·gm·rd·rg^4)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(2·ccc^3·cd^2·gm^3·rd^2·rg^3)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+(16·ccc^3·cd^2·gm·rd^2·rg^3)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+(16·ccc^2·cd^3·gm·rd^3·rg^2)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+(8·ccc·cd^4·gm·rd^4·rg)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+ccc^4·gm^4·rg^4-4·ccc^4·gm^2·rg^4+ccc^4·rg^4-8·ccc^3·cd·gm^2·rd·rg^3-4·ccc^2·cd^2·gm^2·rd^2·rg^2-2·ccc^2·cd^2·rd^2·rg^2+cd^4·rd^4)+ccc·gm·rg·sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-ccc^2·rg^2-cd^2·rd^2)/(sqrt(2)·ccc·cd·rd·rg)),omega=sqrt(-sqrt(-((2·ccc^5·gm^3·rg^5)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2))+(8·ccc^5·gm·rg^5)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+(16·ccc^4·cd·gm·rd·rg^4)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(2·ccc^3·cd^2·gm^3·rd^2·rg^3)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+(16·ccc^3·cd^2·gm·rd^2·rg^3)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+(16·ccc^2·cd^3·gm·rd^3·rg^2)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+(8·ccc·cd^4·gm·rd^4·rg)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+ccc^4·gm^4·rg^4-4·ccc^4·gm^2·rg^4+ccc^4·rg^4-8·ccc^3·cd·gm^2·rd·rg^3-4·ccc^2·cd^2·gm^2·rd^2·rg^2-2·ccc^2·cd^2·rd^2·rg^2+cd^4·rd^4)+ccc·gm·rg·sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-ccc^2·rg^2-cd^2·rd^2)/(sqrt(2)·ccc·cd·rd·rg),omega=-(sqrt(sqrt((2·ccc^5·gm^3·rg^5)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(8·ccc^5·gm·rg^5)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(16·ccc^4·cd·gm·rd·rg^4)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+(2·ccc^3·cd^2·gm^3·rd^2·rg^3)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(16·ccc^3·cd^2·gm·rd^2·rg^3)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(16·ccc^2·cd^3·gm·rd^3·rg^2)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(8·ccc·cd^4·gm·rd^4·rg)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+ccc^4·gm^4·rg^4-4·ccc^4·gm^2·rg^4+ccc^4·rg^4-8·ccc^3·cd·gm^2·rd·rg^3-4·ccc^2·cd^2·gm^2·rd^2·rg^2-2·ccc^2·cd^2·rd^2·rg^2+cd^4·rd^4)-ccc·gm·rg·sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-ccc^2·rg^2-cd^2·rd^2)/(sqrt(2)·ccc·cd·rd·rg)),omega=sqrt(sqrt((2·ccc^5·gm^3·rg^5)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(8·ccc^5·gm·rg^5)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(16·ccc^4·cd·gm·rd·rg^4)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+(2·ccc^3·cd^2·gm^3·rd^2·rg^3)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(16·ccc^3·cd^2·gm·rd^2·rg^3)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(16·ccc^2·cd^3·gm·rd^3·rg^2)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(8·ccc·cd^4·gm·rd^4·rg)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+ccc^4·gm^4·rg^4-4·ccc^4·gm^2·rg^4+ccc^4·rg^4-8·ccc^3·cd·gm^2·rd·rg^3-4·ccc^2·cd^2·gm^2·rd^2·rg^2-2·ccc^2·cd^2·rd^2·rg^2+cd^4·rd^4)-ccc·gm·rg·sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-ccc^2·rg^2-cd^2·rd^2)/(sqrt(2)·ccc·cd·rd·rg),omega=-(sqrt(-sqrt((2·ccc^5·gm^3·rg^5)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(8·ccc^5·gm·rg^5)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(16·ccc^4·cd·gm·rd·rg^4)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+(2·ccc^3·cd^2·gm^3·rd^2·rg^3)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(16·ccc^3·cd^2·gm·rd^2·rg^3)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(16·ccc^2·cd^3·gm·rd^3·rg^2)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(8·ccc·cd^4·gm·rd^4·rg)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+ccc^4·gm^4·rg^4-4·ccc^4·gm^2·rg^4+ccc^4·rg^4-8·ccc^3·cd·gm^2·rd·rg^3-4·ccc^2·cd^2·gm^2·rd^2·rg^2-2·ccc^2·cd^2·rd^2·rg^2+cd^4·rd^4)-ccc·gm·rg·sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-ccc^2·rg^2-cd^2·rd^2)/(sqrt(2)·ccc·cd·rd·rg)),omega=sqrt(-sqrt((2·ccc^5·gm^3·rg^5)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(8·ccc^5·gm·rg^5)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(16·ccc^4·cd·gm·rd·rg^4)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+(2·ccc^3·cd^2·gm^3·rd^2·rg^3)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(16·ccc^3·cd^2·gm·rd^2·rg^3)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(16·ccc^2·cd^3·gm·rd^3·rg^2)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-(8·ccc·cd^4·gm·rd^4·rg)/sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)+ccc^4·gm^4·rg^4-4·ccc^4·gm^2·rg^4+ccc^4·rg^4-8·ccc^3·cd·gm^2·rd·rg^3-4·ccc^2·cd^2·gm^2·rd^2·rg^2-2·ccc^2·cd^2·rd^2·rg^2+cd^4·rd^4)-ccc·gm·rg·sqrt(ccc^2·gm^2·rg^2-4·ccc^2·rg^2-8·ccc·cd·rd·rg-4·cd^2·rd^2)-ccc^2·rg^2-cd^2·rd^2)/(sqrt(2)·ccc·cd·rd·rg),omega=-(%i/(sqrt(ccc)·sqrt(cd)·sqrt(rd)·sqrt(rg))),omega=%i/(sqrt(ccc)·sqrt(cd)·sqrt(rd)·sqrt(rg)),omega=-(%i/(ccc·rg)),omega=%i/(ccc·rg),omega=-(%i/(cd·rd)),omega=%i/(cd·rd)]
		return 0

class VCATO:#Voltage controlled astable tuned oscillator
	def init(self, omega, gm, rd, cd, ccc, rg, ro, ir, lt, mur, mui, ct, epsilonr, epsiloni, c, v_vc):


		self.CCIDA=CCIDA.CCIDA(omega, gm, rd, cd, ccc, rg)

		self.RTNK=PICRT.PICRT(omega, ro, ir, lt, mur, mui, ct, epsilonr, epsiloni, c, v_vc)#Varactor pair for voltage control oscilation

		self.f=omega/(2*math.pi)
		self.omega=omega

		self.RF=0
		self.PH=0
		self.PW=0


	def __init__(self, omega=(2*math.pi)*100*10**6, gm=1, rd=1, cd=0.1*10**(-12), ccc=1*10**(-9), rg=470, ro=1, ir=0, lt=35*10**(-9), mur=1, mui=0, ct=2*10**(-12), epsilonr=1, epsiloni=0, c=0, v_vc=0):

		self.init(omega, gm, rd, cd, ccc, rg, ro, ir, lt, mur, mui, ct, epsilonr, epsiloni, c, v_vc)

	def sIMP(self,omega):#sub impedances calculation
		self.f=omega/(2*math.pi)
		self.omega=omega
		self.CCIDA.sIMP(self.omega)
		self.RTNK.sIMP(self.omega)


	def TF(self):#Transfer function calculation

		self.CCIDA.TF()
		self.RTNK.TF()
		self.TF=(self.CCIDA.TF)/(1-(self.CCIDA.TF*(self.CCIDA.TF+self.RTNK.TF)))

		return self.TF

	def RF(self):
		#Maxima solution:

		return 0




def TEST(argv):
	CIC=PICRT(f=2*math.pi*100*10**6, ir=1, lt=35*10**(-9), mur=1, mui=0, ct=2*10**(-12), epsilonr=1, epsiloni=0, c=0, v_vc=0)

	CIC.sIMP(2*math.pi*100*10**6)
	print("\nCIC.IMP=",CIC.IMP(),"\n")

	print("\nCIC.SRF=",CIC.SRF(),"\n")

	return 0

if __name__ == "__main__":
	TEST("""sys.argv[1:]""")
